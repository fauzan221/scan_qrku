import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blue,
        title: Text("Profile page",
            style: TextStyle(
              color: Colors.white,
            )),
      ),
      body: Column(
        children: [
          customForm(icon: Icon(Icons.credit_card),
              hintText: "NIK",
              helperText: "Masukkan NIK"),
          customForm(icon: Icon(Icons.person_outline_outlined),
              hintText: "Nama",
              helperText: "Masukkan nama sesuai KTP"),
          customForm(icon: Icon(Icons.email_outlined),
              hintText: "Masukan Email"),
          customForm(icon: Icon(Icons.key),
              hintText: "Masukan Password"),
          customForm(icon: Icon(Icons.phone_iphone_rounded),
              hintText: "Masukan Phone"),
          ElevatedButton(
            onPressed: () {
              Fluttertoast.showToast(
                  msg: "This is Center Short Toast",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16.0
              );
            },
            child: Text("Simpan"),
            style: ElevatedButton.styleFrom(
                backgroundColor: Colors.blue
            ),
          ),
        ],
      ),
    );
  }

  Widget customForm({required Icon icon, required String hintText, String? helperText}) {
    return Padding(
          padding: const EdgeInsets.all(4.0),
          child: TextFormField(
            decoration: InputDecoration(
              helperText:helperText,
                hintText:hintText,
                prefixIcon: icon,
                suffixIcon: Icon(Icons.chevron_right),
                border: OutlineInputBorder(
                    borderSide: BorderSide(
                      width: 1,
                      color: Colors.blueAccent,
                    ))),
          ),
        );
  }
 }



